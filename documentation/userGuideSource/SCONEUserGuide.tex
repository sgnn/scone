%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage[disable]{todonotes}
%\usepackage{todonotes}
\usepackage[version=3]{mhchem} % Package for chemical equation typesetting
\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements '

\usepackage[nounderscore]{syntax} % used for the ebnf
\usepackage{hyperref}
\usepackage{xspace}
\usepackage{float}
\usepackage{longtable}
\usepackage{verbatim}

%\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

\newcommand{\sigm}[1]{#1} %{$\phi$({#1})}
\newcommand{\scone}{\textsc{Scone}\xspace}
%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{\textbf{Sc}enario-based \textbf{On}tology \textbf{E}valuation (\scone) \newline
	User Guide } % Title

\author{Fabian {Neuhaus}} % Author name

\date{\today} % Date for the report

\begin{document}
\listoftodos
\newpage

\maketitle % Insert the title, author and date
\tableofcontents

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Purpose of this document}
This document describes \scone{}, a tool for \textbf{Sc}enario-based \textbf{On}tology \textbf{E}valuation.
``\scone{}'' is the name for a methodology, namely an approach to ontology engineering that relies on  requirements-based tests to drive the development process. ``\scone{}'' is also the name of a language that was to developed for writing down requirements of ontologies. Lastly, ``\scone{}'' is a name for a tool that translates requirements for ontologie into behavioral tests and executes them. Thus, \scone enables the evaluation of the behavior of ontologies. 
%

%  which allows the evaluation of the behavior of ontologies.
% It provides some background information about the design and intended use for
% the \scone language, and it specifies its syntax and its semantics.

 
 
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Introduction}

In the Ontology Summit 2013 Communique ontology evaluation is defined as follows: 
%
\begin{quote}
Ontology evaluation consists of gathering information about some properties of an ontology, comparing the results with a set of requirements, and assessing the ontology’s suitability for some specified purpose.\footnote{F. Neuhaus et al: Towards ontology evaluation across the life cycle,  Applied Ontology 8(3): 179-194 (2013).}
\end{quote}
%
Hence, according to this definition, requirements play a central role in the evaluation of ontologies. They are used to evaluate whether an ontology meets its expectations 
and whether it is able to play its intended role within a given information system.  In short, 
requirements provide the measurement yard  for the fitness of an ontology. 
%The Ontology Summit 2013 Communique distinguishes between 5 kinds or requirements: intelligibility, fidelity, craftsmanship, deployability, and fitness.  

In order to evaluate the fitness of ontologies Gr{\"u}ninger and Fox have suggested to  capture requirements as combination of \emph{scenarios} and \emph{competency questions}\footnote{M Gr{\"u}ninger, MS Fox (1995): Methodology for the Design and Evaluation of Ontologies. Workshop on Basic Ontological Issues in Knowledge Sharing, IJCAI-95, Montreal.}. Their approach can be summarized as follows:

\begin{itemize}
	\item  A requirement is captured within scenario, which is a narrative text in a natural language. The scenario describes a possible state of the world and raises a set of competency questions. The answers to these competency questions should follow logically from the scenario -- provided the knowledge that is supposed to be represented by the ontology. 
	\item Both the scenarios and the questions are formalized within the same knowledge representation language. 
 	\item A theorem prover is used to try to prove the answer to the formalized competency questions based on the ontology under test and the formalized  scenario. 
	\item If the theorem prover is able to answer the competency questions correctly, the 
	ontology meets the requirement.  
\end{itemize}

 
This methodology of scenario-based ontology evaluation 
has several major advantages.

Firstly, because the scenarios and the competency questions are written in a narrative text in a natural language, they can be written, reviewed, and maintained by the people who are not
professional ontologists. This means that it is possible to specify the  requirements together with a stakeholder in a language the stakeholder understands, and it allows to generate
executable tests, which show whether these requirements are met. 

Secondly, since, typically, the requirements are specified in many scenarios, and any scenario is linked to multiple competency questions, the ratio of competency questions that are answered correctly compared to all competency questions provides a good metric for progress during development. Often new ontology developers are not sure when to stop to add new axioms to an ontology. Using competency questions provides a natural answer: assuming that the requirements are specified properly, then an ontology that answers all competency questions correctly covers the domain appropriately to meet the requirements -- thus, no additional information needs to be represented.   

Thirdly, because the competency questions allow to track progress and can be evaluated automatically, it is easy to use them for regression testing. Hence, it is possible to automatically detect whether an changes to an ontology broke some existing feature and, thus, catch errors before a new version of an ontology is released.     


Unfortunately, at this stage there is no convenient ways for ontology engineers to evaluate their ontologies with this methodology. One barrier is the formalization step from the 
natural language text to the formalized scenarios and formalized competency questions, which puts an extra burden on the ontology engineers. Another reason is that currently there are no tools that integrate the ontology evaluation step within the work flow of ontology development. For this reason, the Ontology Summit 2013 Communique recommends: 

\begin{quote}
	Evaluation should be conducted against carefully identified requirements; these requirements depend on the intended use of the ontology and its operational environment. For this reason, we recommend the development of integrated ontology development and management environments that support the tracking of requirements for, and the evaluation of, ontologies across all phases of their development and use.\footnote{F. Neuhaus et al: Towards ontology evaluation across the life cycle,  Applied Ontology 8(3): 179-194 (2013)}\end{quote}
% F. Neuhaus et. al: “Towards ontology evaluation across the life cycle” Applied Ontology 8 (3), 179-194. 

The purpose of \scone is provide an important building block for developing such an integrated ontology development and evaluation environment. \scone is a form of restricted English that allows to express requirements for ontologies. These
 requirements can be automatically translated into Distributed Ontology, Model, and Specification (DOL) libraries.\footnote{DOL is a response to the Object Management Group (OMG) Ontology, Model and Specification Integration and Interoperability Request for Proposal and is currently in the finalization phase. More information and the latest version of the draft standard is available at \url{https://github.com/tillmo/DOL}.} The DOL libraries capture the content of the scenarios and the affiliated competency
 questions in a formal way. These DOL files can be executed by proof management systems such as Hets\footnote{\url{http://www.informatik.uni-bremen.de/agbkb/forschung/formal_methods/CoFI/hets/index_e.htm}}, which allows their use 
 as behavioral tests.  Since we plan to integrate \scone into the ontology 
repository Ontohub\footnote{\url{www.ontohub.org}}, it will be convenient for ontology developers to integrate the use of these behavioral tests within their development workflow. 



 %
 %
 % The Distributed Ontology, Model, and Specification Language (DOL) is a declarative language, which enables, among other features, the representation of relationships between ontologies. It is very well suited as a foundation for an Ontology eVAluation Language (OVAL). We propose to reuse the relevant features of DOL and extend the language for the purpose of ontology evaluation.
 % OVAL will be based on the methodology as proposed  In particular, it will embrace a systems view on ontology engineering, which recognizes that ontology evaluation is dependent on the requirements of the information systems that the ontology is part of.
 % Thus, one major focus of OVAL is going to be the ability of the language to express competency questions (see M Grüninger, MS Fox: The role of competency questions in enterprise engineering. In Benchmarking—Theory and Practice, 1995 – Springer). Competency questions are used to ensure that the axioms of a given ontology are rich enough to meet its requirements. Since these requirements are often expressed within the context of an application scenario, the representation of these scenarios is an important part of the ontology evaluation and, thus, needs to be supported by the OVAL.
 % While competency questions measure whether an ontology is sufficiently axiomatized, ontologies may also be over-axiomatized, that is they may contain axioms that are too restrictive. OVAL will support ways to test for these kind of errors with the help of scenarios and model checkers.
 % Since DOL is not written for a particular ontology language, OVAL will be language independent as well, and, thus, be applicable to both Common Logic and the Web Ontology Language (OWL).
 % The specification of OVAL will contain the syntax and the semantics of OVAL. The grammar will be specified as EBNF grammar. It’s semantics will provided by natural language text and a mapping to DOL (when possible).
 %



%----------------------------------------------------------------------------------------
%	SECTION 
%----------------------------------------------------------------------------------------

\section{Overview and Example}


The \scone language is an ontology evaluation specific extension of Gherkin\footnote{\url{https://github.com/cucumber/cucumber/wiki/Gherkin}}. Gherkin is the language of
 Cucumber\footnote{\url{https://github.com/cucumber/cucumber/wiki}},
which is a widely used tool that supports  behavior-driven development (BDD) of software by  
executing plain-text functional descriptions as automated tests. 


\begin{figure}
%\begin{small}
\verbatiminput{familyRelationship.feature}
%\end{small}
\caption{\scone specification example \label{fig:ex}}
\end{figure}

A typical example for a \scone file is presented in Figure \ref{fig:ex}. The file contains a description of the feature of the ontology that is tested. This section contains free text
and is not utilized by the tool for testing purpose. The background section contains information that is shared across all scenarios. In this example, the language is set to OWL and 
the ontology geneology.owl is loaded. The background is followed by three different scenarios. Each scenario has a title and may contain a descriptive text. Further, Tthese scenarios may contain assumptions, which are marked by the "Given" keyword. In the first scenario the assumptions introduce Dora, her father Chris, her grandmother Amy, and her aunt Berta. (The keyword "And" is used instead of "Given" in order to improve the readability of the text, there is no functional difference.) The purpose of this scenario is to test the relationship between "parent of" and "older than".  Thus, the scenario contains four 
competency questions about the age of Dora compared to the people in the scenario.  These competency questions are introduced by the "Then" keyword (or, afterward, "And"). They are describing the intended behavior of the ontology given the scenario. E.g., one is supposed to be able to infer that Amy is older than her granddaughter, but we should not be able to logically infer that Berta is older than her niece. 



The order of assumptions and competency questions within a scenario is not important. This makes it possible to state the competency questions after all relevant assumptions have been introduced. 
For example, in the second scenario the two first assumptions are sufficient for inferring that Sue is a grandparent of Mary. By adding a new assumption about John's gender, we can add a new competency question that tests the definition of the father relationship. This example shows how one can build a scenario step-by-step by alternating between adding new assumptions and new competency questions.

Most of the assumptions are declarations of facts about individuals; e.g., ``John is male'' or ``John is the father of Mary''. However, the second example in Fig. \ref{fig:ex} contains a definition of ``mother''. Definitions are only one kind of many types of complex assumptions that \scone supports (see section \ref{sec:syntax}). However, since \scone is a controlled English it is less expressive than, for example, OWL. For the rare cases where its expressivity  is not sufficient, \scone allows to directly include axioms in the given ontology language. 


There are four different kinds of competency questions support: (1) a piece of information should be inferable from the scenario, (2) a piece of information should not be inferable, (3) the scenario is consistent, and (4) the scenario is inconsistent. 

%----------------------------------------------------------------------------------------
%	SECTION 
%----------------------------------------------------------------------------------------

\section{Architecture of \scone{}}
\begin{figure}
\includegraphics[scale= 0.7]{architecture.pdf}
\caption{System Architecture of \scone{} \label{fig:architecture}}
\end{figure}

Figure \ref{fig:architecture} provides an overview over the implementation of \scone{}. The inputs for \scone{} are one or more scone files and, typically, one ontology that is under test. (Theoretically, more than one ontology can be tested.) Each \scone{} file tests a feature with the help of several scenarios. 

Since \scone{} is an extension of Cucumber, Cucumber runs the testing process. While Gherkin provides the general structuring of a \scone{} file into features, background, scenarios, it does not contain any keywords that are particularly relevant to ontology development. Thus, Cucumber hands over \scone{} sentences to the \scone{} parser, which analyses \scone{} sentences and competency questions. The result is used to generated a DOL-file, which contains some embedded text in the chosen ontology language (e.g., OWL). After the DOL file is generated, Cucumber calls Hets in order to execute the proof obligations within the DOL-file. The result is reported back to Cucumber, which generates a report to the user. 
%----------------------------------------------------------------------------------------
%	SECTION 
%----------------------------------------------------------------------------------------



\section{Syntax of the \scone Language}\label{sec:syntax}
\shortverb{\|}
\setlength{\grammarindent }{2cm}

\subsection{Main Structure}

\begin{grammar}

<scone-spec> = <feature>, [<background>], \{<scenario>\};

<feature> = \{<tag>\}, "Feature:", <title>, EOL, <narrative>;

<title> = string  

<narrative> = string  

<background> = "Background:", [<language setting>], \{<import>\}; 

<tag> = a space delimited string starting with '@';

<language setting> = "Language", <language ID>;

<language ID> =  "%OWL" | "%CommonLogic" 

<import> = "* Test", ["the ontology"], <uriref>;



\end{grammar} 
\subsection{Scenarios}
Scenarios are built from a set of assumptions and a set of tests. Each assumption involves a sentence. A test is either 
a consistency test or a inference test. Any inference test involve a sentence.\footnote{To enable a correct mapping to OWL 
 particular sentences are not allowed to be part of inference tests, this is why we introduced \emph{infer sentence} as 
a syntactical subcategory. This limitation can be omitted for CLIF or other langauges that are expressive enough to enable the 
required form of existential quantification.}

\begin{grammar}

%<scenario> = {<tag>}, "Scenario:", , [<language setting>], \{<import>\},\newline \{<assumption>\}, \{<test>\};  

<scenario> = \{<tag>\}, <scenario head>, <scenario body>;

<scenario head> = "Scenario:", <title>, EOL, <narrative>, [<language setting>]; 

<scenario body> = \{<assumption>\}, \{<test>\};  


<assumption> = "Given", <sentence>, ["."];

<test> =  "Then", <inference test> | <consistency test>, ["."];

<inference test> = ["don't"], "infer", ["that"], <infer sentence>; 

<consistency test> =  "the scenario is", "consistent" | "inconsistent";  

\end{grammar}



\subsection{Sentences}

\begin{grammar}

<sentence> =   <definition> | <proposition>  | <fact>  | <source>;

<infer sentence> =   <definition> | <universal> | <fact>  | <source>;

%%%%%%%%%%%%%%%%%%%%
<definition> = (<pos class>, "is defined as", <class expression>) \alt  				
			(<class name>, "is enumerated as", <indiv>, \{"and", <indiv>\});

%%%%%%%%%%%%%%%%%%%%
<proposition> = 	<universal> | <particular>;

<universal> = <universal positive> | <universal negative>; 

<universal positive> = ("every", <class name>, "is", <class expression>) \alt
					(<class name>, "is a subclass of", <class expression>);

<universal negative> = ("no", <class name>, "is a", <class expression>) \alt
					(<class name>, "and", <class name>, \{"and", <class name>\}, "are disjoint");
					
<particular> = "some", <class name>, ("are" | "aren't"),  <class expression>; 					

%%%%%%%%%%%%%%%%%%%%

<fact> = <instance> | <relation> | <equation> | <different>;

<instance> = <indiv name>,  "is" | "isn't", <class expression>;

<relation> = <indiv name>, <predicate phrase>, <indiv name>;  

<equation> = <indiv name>, "is" | "isn't", "the same as",  <indiv name>; 

<different> =  <indiv name>, "and", <indiv name>,  \{"and", <indiv name>\}, "are different";
 
 %
 %    Given John is the same as Jill.
 %    Given Jill is not the same as Mary.
 %    Given Jill is different from Mary.
 %


%%%%%%%%%%%%%%%%%%%%

<source> = <language ID>, <delimiter>, <source body>, <delimiter>;


<source body> = some syntactically valid expressions in the identified ontology language  

<delimiter> = EOL, " \"\"\" ", EOL;

\end{grammar}

\subsection{Terms}


\begin{grammar}

<class expression> = <class atom> | <conjunction> | <disjunction> | <qualified class>
%pos class = positive class

<pos class> = ["a" | "an"],  <class name>;

<class atom> = ["not"], <pos class>;

<conjunction> = <pos class>, "and", <class atom>, {"and", <class atom>}\alt
				"neither", <pos class>, "nor", <pos class>, \{"nor", <pos class>\};

<disjunction> = <class atom>, "or", <class atom>, \{"or", <class atom>\};   
% never start with negated class unless all classes are negated 

<qualified class> = <pos class>, [","], "who" | "which" | "that", <qualifier>;  


% <qualifier> = ("is", <class atom> | <conjunction> | <disjunction>) \alt
% 			  (<predicate phrase>, [<quantifier>], <pos class>);


<qualifier> = ("is", <class expression>) \alt
			  (<predicate phrase>, <quantifier>, <class expression>); 

			  

<quantifier> = "some" | "only" 


<predicate phrase> = (<predicate open>, <predicate name>) \alt
					("is" | "isn't", ["a" | "an" | "the"], <predicate fragment>, <predicate end>);
					
<predicate open > = "does" | "doesn't" | ("has" | "hasn't", "as");

<predicate end>	= "of" | "than" | "to" | "on" | "in";		
						
<class name> = <qname> (see below)

<predicate name> = <qname> (see below)

<predicate fragment> = <qname> (see below)

<indiv name> = <qname> (see below)

<uriref> = "<", <relativeURI>, ">" (see below)

% <qname> =  <boolean>
%
% <restriction> =
\end{grammar}

The grammar for the \scone language does not explicitly show white spaces. White space 
is required between two terminals or non-terminals. 
The syntactic categories \emph{qname} and \emph{uriref} are defined as strings matching 
the \emph{qname} and \emph{uriref} production within the W3C Turtle 
Specification.\footnote{See \url{http://www.w3.org/TeamSubmission/turtle/}.} The
prefixes and the local parts  of qnames must not match any of the keywords. Further, 
the prefix of a qname must not be `scn'.  

A special case are predicate expressions like `father of' or `larger than'. Here the predicate 
is divided up in two different words. Of course, this is a more general phenomenon in the English 
langauge, which cannot be easily supported in a controled English like \scone. However, to provide
at least limited suppport we introduced prerdicate end as syntactic category. The words in this 
category (e.g., `of' and `than') serve as keyword. The predicate is generated by concatenating 
the word that preceeds the keyword with the keyword by an underscore. (E.g., 
`man who is father of some child' is turned into `man and father$\_$of some child'.)

 



%
%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------


\section{Semantics of the \scone Language}

\subsection{\scone and Gherkin}

\scone extends Gherkin by providing a set of step definitions, which are relevant for ontology evaluation. 
This enables us to use Cucumber for the execution of \scone tests. Since writing new step definitions 
requires only some basic knowledge of Ruby and regular expressions%
\footnote{\url{https://github.com/cucumber/cucumber/wiki/Step-Definitions}}, \scone is easily
extendable and customizable. 
 

Since \scone is an extension of Gherkin, the main structure of a \scone document (i.e., 
feature description, background, scenarios) is inherited
from Gherkin. Further, the ability to tag scenarios and features as well as 
the keywords "*", "Given", "Then". "And", which are used to separate individual steps, are 
provided by Gherkin. Since Gherkin is well documented\footnote{\url{https://github.com/cucumber/cucumber/wiki/Gherkin}} we won't 
explain the semantics of these aspects of \scone in this document in detail, but only to 
the degree to which it is necessary to understand the rest of \scone.   


\subsection{Mapping to DOL}

The semantics of a \scone file is provided by its mapping to a DOL file. To consider this mapping, let's reconsider the structure of a \scone file: it consists of (i) a description of the feature of the ontology
that is tested (2) a setup of the backup, and (3) a number of scenarios, which consists of assumptions and 
competency questions.

The feature description is intended to document the purpose of the tests for humans, but for the formal semantics of the \scone file it is irrelevant. The background captures information that is shared across all scenarios in the file. This is convenient, but, from a theoretical perspective, does not add anything to the expressivity of the language, because it would be possible to add the directly to each scenario in the file. Thus, without any loss of generality we can ignore the background and focus on the scenarios. 

Since the order of the statements within a scenario does not matter, any scenario is represented by the  pattern in Fig \ref{fig:schema}.\footnote{Note that we assume here  exactly one language setting. The grammar actually does not require that. If no language setting is present, the default language is OWL. In the case of more than one language setting, we consider only the last one.} 
\begin{figure}[H]
\begin{verbatim}  
Scenario: 
  * Language language_ID

  * Load the ontology <uriref_1> 
  ...
  * Load the ontology <uriref_k> 
  
  Given assumption_1 
  ...
  Given assumption_n

  Then comp_question_1 	
  ...
  Then comp_question_m 	 
\end{verbatim}
\caption{Generic schema of a \scone scenario \label{fig:schema}}
\end{figure}

The semantics of the scenario is provided by a translation to a DOL file (see Fig. \ref{fig:trans}). Its first part defines an ontology  \emph{importsAndAssumptions} that aggregates all information from the imported ontologies and the formalization of the assumptions. The \emph{formalization}-function that maps  \scone sentences to sentences in the ontology language obviously depends on the chosen language. In the case of languages like OWL and Common Logic, the definition of the corresponding \emph{formalization}-function is straight forward.

\begin{figure}[H]
\begin{verbatim}  
logic language_ID

ontology ont_1 =  <uriref_1> 
...
ontology ont_k =  <uriref_k> 

ontology combinedImports = 
    ont_1 and ... and ont_k 

ontology importsAndAssumptions = combinedImports	
	then  
    formalization[assumption_1]
    ...
    formalization[assumption_n]
  end 



ontology cq1 = importsAndAssumptions then 
    %implies formalization[comp_question_1] end
...
ontology cqk = importsAndAssumptions then 
    %implies formalization[comp_question_k] end
\end{verbatim}
\caption{Translation to a DOL file \label{fig:trans}}
\end{figure}

The second part of the file states the proof obligations between the ontology \emph{importsAndAssumptions} and the competency questions. (Each competency question yields one proof obligation.)	The formalization of the competency question depends on its kind. 
\begin{itemize}
	\item If the competency question is a test of the form "Then infer $<$sentence$>$", then the \emph{formalization}[comp\_question\_i] equals  \emph{formalization}[$<$sentence$>$].
	\item  If the competency question is of the form "Then the ontology is inconsistent", then \emph{formalization}[comp\_question\_i] equals a representation of falsum in the chosen language.\footnote{E.g., 
"(and (p) (not (p))))" in Common Logic and "Class: owl:Thing SubClassOf owl:Nothing" in OWL.} 
	\item  The representation of the two other cases is difficult in the current version of the syntax of DOL. We are in discussion with the DOL working group, which intends to add the necessary features to DOL. 
	
	Until this happens, 
	we can evaluate competency questions of the type "Then the ontology is consistent" operationally by calling a consistency checker on the ontology {\emph{importsAndAssumptions}}.  In the case of OWL ontologies, competency questions of the type "Then don't infer $<$sentence$>$"  can be evaluated operationally by attempting to infer the $<$sentence$>$" from the ontology. Because OWL is decidable, a failed attempt shows that the $<$sentence$>$" cannot be inferred, and, thus, the competency questions is passed successfully. In the case of Common Logic ontologies,  competency questions of the type "Then don't infer $<$sentence$>$"  can be evaluated operationally by 
	 calling a consistency checker on the ontology that is the result of extending {\emph{importsAndAssumptions}} by  (not \emph{formalization}[$<$sentence$>$]). 
 
\end{itemize}

\section{Mapping of Sentences and Terms to OWL}

The following table maps \scone expressions to OWL expressions (in Manchester syntax). This mapping provides the semantics of \scone sentences. We will use `c', `p', `i' (possibly with indices) as symbols for class names, predicate names, and individual names, respectively. `X' and `Y' are used for class expressions. `PredicatePhrase' for predicate phrases. We simplify the mapping by ignoring grammatical variants, such as the use of articles (e.g., `a', `the') or the use of plurals. 
%\caption[Mapping from \scone sentences to OWL Manchester Syntax} \label{tbl:semantics} \\

\begin{center}
\begin{longtable}{|p{0.45 \textwidth}|p{0.52 \textwidth}|}
\caption{Definition of the \emph{formalization} mapping for OWL}\\
\hline
\textbf{\emph{formalization}[$<$scone$>$] =} & \textbf{OWL}\\
\hline
\endfirsthead
\multicolumn{2}{c}%
{\tablename\ \thetable\ -- \textit{Continued from previous page}} \\
\hline
\textbf{\emph{formalization}[$<$scone$>$] =} & \textbf{OWL}\\
\hline
\endhead
\hline \multicolumn{2}{r}{\textit{Continued on next page}} \\
\endfoot
\hline
\endlastfoot
 \hline \emph{Definitions and propositions} &\\ \hline
[c is defined as Y] & Class: [c] EquivalentTo: [Y] \\ \hline
[c is enumerated as i$_1$ and i$_2$ and \ldots i$_n$] & [c] EquivalentTo: \{[i$_1$],\ldots, [i$_n$]\}\\ \hline
[Every c is Y] & Class: [c] SubClassOf: [Y]\\ \hline  
[c is a subclass of Y] & Class: [c] SubClassOf: [Y]\\ \hline  
[No c is a Y] &   Class: [c] DisjointWith: [Y]  \\ \hline
[c$_1$ and c$_2$ and \ldots c$_n$ are disjoint] &   DisjointClasses: [c$_1$],\ldots, [c$_n$]  \\ \hline
[Some X are Y] & Individual: w Types: [X], [Y] $^\dag$\\ \hline
[Some X aren't Y] & Individual: w Types: [X], not ([Y]) $^\dag$\\ \hline \hline
% \multicolumn{2}{l}{\emph{Facts:}}  \\ 
%
%\hline
\emph{Facts} &\\ \hline
[i is a Y] & Individual: [i] Types: [Y]\\ \hline
[i isn't a Y] & Individual: [i] Types: not [Y]\\ \hline
[i PredicatePhrase j] & Individual: [i] Fact: [PredicatePhrase] [j] \\ \hline
[i is the same as j] &  SameIndividual: [i], [j]\\ \hline  
[i$_1$ and i$_2$ and \ldots i$_n$ are different] & DifferentIndividuals: [i$_1$], [i$_2$], \ldots , [i$_n$]\\ \hline
%
\hline
%
\emph{Class expressions} &\\ \hline
[not c] & not [c]\\ \hline
[c and X$_1$ and  \ldots and X$_n$]  & ([c] and [X$_1$] and  \ldots and [X$_n$]) \\ \hline
[neither X$_1$ nor  \ldots nor X$_n$]  & (not [X$_1$] and  \ldots and not [X$_n$]) \\ \hline
[X$_1$ or  \ldots or X$_n$]   &  ([X$_1$] or \ldots or [X$_n$]) \\ \hline
[c, which is X] &  [c] and [X]  \\ \hline 
[c$_1$, which PredicatePhrase only c$_2$] & ([c$_1$] and [PredicatePhrase] only [c$_2$])\\ \hline
[c$_1$, which PredicatePhrase c$_2$] & ([c$_1$] and [PredicatePhrase] some [c$_2$])\\ \hline
[c$_1$, which PredicatePhrase some c$_2$] & ([c$_1$] and [PredicatePhrase] some [c$_2$])\\ \hline 
%
 \hline
%
\emph{Predicate phrases} &\\ \hline
[does p] & [p]  \\ \hline
[doesn't p] & not [p]  \\ \hline
[is predFragement predEnd] &  [p] \newline where p = predFragment$\_$predEnd 
\footnote{
As discussed in section \ref{sec:syntax}, in these cases the predicate name is the result of concatenating the predicate fragment with the predicate end. E.g., the predicate fragment ``father'' together with the predicate end ``of'' yields ``father$\_$of''.}
 \\ \hline
[isn't predFragement predEnd] & not [p] \newline where p = predFragment$\_$predEnd  \\ \hline
%
\hline
%

\emph{Names} &\\ \hline
class name: [c] &  \sigm{c}\\\hline
predicate name: [p]& \sigm{p} \\ \hline
indiv name: [i] & \sigm{i} \\

\end{longtable}
\footnotesize{$^\dag$ w is a new individual}
\end{center}

\section{Installation}
To run Scone, you need an environment configured properly. We use Vagrant to build a Virtual Machine that contains everything you need, see the Scone-Vagrant repository. This project can be find at:

\url{https://bitbucket.org/malefort/scone-vagrant}

The installation of \scone{}-vagrant is explained in detail on the homepage of this project. Additional information can be found on the wiki of the scone{}-vagrant project. 

\url{https://bitbucket.org/malefort/scone-vagrant/wiki/Home}


After the installation of the vagrant environment, one can retrieve \scone{} from the following HG repository: 

\url{https://bitbucket.org/malefort/scone}

The installation of \scone{} is explained in detail on the homepage of this project. Additional information can be found on the wiki of the \scone{} project. 

\url{https://bitbucket.org/malefort/scone/wiki/Home}

This project contains a bug-tracker, which can be used to report bugs and ask for feature requests. 

%
% <class expression> = <class atom> | <conjunction> | <disjunction> | <qualified class>
%
% <pos class> = ["a" | "an"],  <class name>;
%
% <class atom> = ["not"], <pos class>;
%
% <conjunction> = <pos class>, "and", <class atom>, {"and", <class atom>}\alt
% 				"neither", <pos class>, "nor", <pos class>, \{"nor", <pos class>\};
%
% <disjunction> = <class atom>, "or", <class atom>, \{"or", <class atom>\};
% % never start with negated class unless all classes are negated
%
% <qualified class> = <pos class>, [","], "who" | "which" | "that", <qualifier>;
%
% <qualifier> = ("is", <class atom> | <conjunction> | <disjunction>) \alt
% 			  (<predicate phrase>, [<quantifier>], <pos class> );
%
%
% <quantifier> = "some" | "only"
%
%
% <predicate phrase> = (<predicate open>, <predicate name>) \alt
% 					("is" | "isn't", ["a" | "an" | "the"], <predicate name>, <predicate end>);
%
% <predicate open > = "does" | "doesn't" | ("has" | "hasn't", "as");
%
% <predicate end>	= "of" | "than" | "to" | "on" | "in";
%

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------
%
% \bibliographystyle{apalike}
%
% \bibliography{sample}

%----------------------------------------------------------------------------------------


\end{document}