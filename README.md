# Scone Project

Scone is a tool that supports scenario-driven development for ontology
engineers. Ontologies are used to represent knowledge in many
industries, including medical, chemical, pharmaceutical, and financial.

Scone is an effort to apply best practices from behavior-driven
development (BDD) in software engineering to ontology development. The
goal is to enable ontology developers to record their requirements in a
way that

  - is accessible to their clients and maintainable by domain experts
  - provides automatically generated behavioral tests
  - provides metrics for ontology developers that provide a useful
    measure of progress and quality

More detail can be found in our [user guide][0].

## Installing Scone

Scone is still in an early development stage, so its installation
process is a little more complicated. First off, it's not yet packaged
so you have to install it from source. You also need a few dependencies,
some of which aren't entirely obvious. Finally, after installation,
there are a few things you have to set up manually in order to run
Scone.

The steps necessary in order to install Scone are the following.

### Installing Scone's Dependencies

#### `Docker`

If trying `docker --version` in a terminal window doesn't give you an
error, this dependency is satisfied.

Otherwise, simply doing `sudo apt install docker.io` has been reported
to work on reasonably recent Ubuntu derivatives. If that fails for you,
or if you are on a different platform, the Docker website has
[installation instructions][2] for various platforms

#### `Ruby`

If trying `ruby --version` in a terminal window doesn't give you an
error, this dependency is satisfied.

Otherwise, follow Ruby's [installation instructions][3].

#### `Bundler`

If trying `bundle --version` in a terminal window doesn't give you an
error, this dependency is satisfied.

Otherwise, follow Bundler's [installation instructions][4].

#### `Git`

If trying `git --version` in a terminal window doesn't give you an
error, this dependency is satisfied.

Otherwise, get it from Git's [download page][5].

### Installing Scone/Setting Up It's Environment

Create a directory in which Scone's source code and other installation
data will reside. We'll refer to this directory as `SCONE_ENVIRONMENT`
and whenever you see `$SCONE_ENVIRONMENT` in a code block or command,
you should replace `$SCONE_ENVIRONMENT` with the *absolute* path to the
directory you just created.

#### Getting Scone's Source Code

Run

```
git clone https://gitlab.com/sgnn/scone.git $SCONE_ENVIRONMENT/scone
```

in a terminal. This will create a subdirectory called `scone` containing
Scone's source code in the `SCONE_ENVIRONMENT` directory.

#### Installing the Hets Docker Container

Run

```
mkdir $SCONE_ENVIRONMENT/hets
cd $SCONE_ENVIRONMENT/hets
docker pull spechub2/hets
mkdir hets_data
```

in a terminal.

#### Setting up Scone's Environment

Run

```
ln -s "$SCONE_ENVIRONMENT/hets/hets_data" /tmp/scone
cp -R "$SCONE_ENVIRONMENT/scone/resources/" /tmp/scone
cd "$SCONE_ENVIRONMENT/scone"
bundle install --path=vendor/bundle
```

in a terminal.

### Running Scone

Scone needs a running instance of the Hets REST interface in order to
work properly. You can start one by running

```
docker \
  run \
  -d -p 8000:8000 \
  --mount type=bind,source=$SCONE_ENVIRONMENT/hets/hets_data,target=/data/ \
  --name="hets_docker_container" \
  --rm \
  spechub2/hets:latest
```

in a terminal window. Once you've done this, you can run Scone by
changing to the `lib` directory in Scone's source repository, which
contains some example features, via  
`cd $SCONE_ENVIRONMENT/scone/lib`.  
Once you've done this, you're finally ready to run Scone via
`bundle exec ../bin/scone`.

Don't forget to shut down the Hets server via
`docker stop hets_docker_container`
once you're done trying out Scone.

[0]: https://bitbucket.org/malefort/scone/raw/563c2f0dfb49e1d08939601ec046567dcf7f88c0/documentation/SCONEUserGuide.pdf
[2]: https://docs.docker.com/install/
[3]: https://www.ruby-lang.org/en/documentation/installation/
[4]: https://bundler.io/#getting-started
[5]: https://git-scm.com/downloads
