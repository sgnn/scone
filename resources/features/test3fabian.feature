@given
Feature: Scone Test

Background:

Scenario: Ramblings of an idiot (universal Propositions)

Given No dog is a mammal and a bird.
Given No dog is a mammal and a bird and green.
Given No dog is a mammal or a bird or green.
Given No dog is a mammal, which is green
Given No dog is a mammal that is green
Given No dog is a mammal who is green.
Given No dog is a mammal, who is an father of some cat.
Given No dog is a mammal, who is located on  some trees.  
Given No dog is a mammal, that does love some cats.
Given No dog is a mammal who does love only cats.
Given No dog is a mammal that does love cats.
Given Dog and Cat are disjoint.


#classAtom
Given John is a Mammal
Given John is not a Vampire

#conjunction

Given John is Mammal and Human
Given John is a Mammal and Human
Given John is a Mammal and a Human
Given John is a Mammal and not an Animal.

#disjunction
Given John is a Human or a Dog

#qualified class
Given John is a Dolphin, which is a Fish
Given John is a Dolphin, which is a Fish and a Mammal and not a Bird
Given John is a Dolphin, which does not fly
Given John is a Dolphin which does not fly


#universal negative
Given Dog and Cat and Bird are disjoint.


Given Every dog is a mammal.
Given Every DOG is a mam_mal.
Given Every :dog is a ow:mammal

Scenario: Ramblings on particular propositions

Given that some dogs are cute.
Given that some dogs are cute and lively.
Given that some cats aren't pretty.
Given that some cats are animals, which don't eat meat.
Given that some cats aren’t animals, which do not eat meat.

Scenario: Empty

Scenario:

Scenario: Silly Definitions
Given A foo is defined as sleepy and not hungry.
Given A foo is defined as sleepy or not hungry.
Given A foo is defined as neither sleepy nor hungry.
Given A foo is defined as bear that is hungry or not sleepy.
Given A foo is defined as bear that is hungry and not sleepy.
Given foo is enumerated as Minna and Maya.
Given foo is enumerated as Minna and Maya and Maria.

Then the ontology is inconsistent.


Scenario: Verbatim

   Given %owl
   """
   Class: Nut
   Class: Peanut
       SubClassOf: Nut   
   """

Scenario: Verbatim squared

   Given %owl
   """
   Class: Nut
   Class: Peanut
       SubClassOf: Nut   
   """

   Given %owl
   """
   Class: Tree
   Class: Birch
       SubClassOf: Nut   
   """