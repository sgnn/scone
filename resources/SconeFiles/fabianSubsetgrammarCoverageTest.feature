Feature: Showcase for a simple language for ontology evaluation 
This file works with Fabian's first implementation. 
 

  Background: 
  * Load the ontology <https://ontohub.org/repositories/sandbox/08b73a7d12865296639537976d2cda0e1d94eebe/download/cycle.owl> 

  Scenario: 

    # Categorical propositions   
    Given Every monkey is an animal. 
    Given No cat is a dog.
    Given Some dogs are cute. 
    Given Some dogs are not cute. 

    # Ontology speak 
    Given Human is a subclass of animal.
    Given Human and cat are disjoint. 

    # Facts 
    Given Joe is an prefix:Human. 
    Given This_car isn't an animal.

    Given John does love Mary.
    Given John does not love Sue 

    Given John is the same as Jill. 
    Given Jill isn't the same as Mary. 
    Given Jill and Mary are different. 

    # definitions 

    Given foo is defined as bar. 
    Given parent is defined as mother or father. 
    Given bachelor is defined as man and unmarried. 
    Given associate is defined as employee or contractor or guest.  
    Given a mother is defined as a parent, who is female. 
    Given a car is defined as a vehicle, which drives_on land. 
    Given a car is defined as a vehicle, which drives_on some street. 
    Given a vegetarian is defined as a person, who eats only meat. 
 
    #  OWL source 
    Given %owl 
    """
    Class: Nut 
    Class: Peanut 
        SubClassOf: Nut   
    """

    Given %OWL 
    """
    Class: animal
    Class: dog
    """
    

    ##  
    Then infer John is an animal. 

    
    #  TODO
    # Given A foo is defined as sleepy and not hungry. 
    # Given A foo is defined as sleepy or not hungry. 
    # Given A foo is defined as neither sleepy nor hungry. 
    # Given A foo is defined as bear that is hungry or not sleepy. 
    # Given A foo is defined as bear that is hungry and not sleepy. 
    # Given A foo is enumerated as Minna and Maya. 

   