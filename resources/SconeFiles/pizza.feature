Feature: Pizza Types
In order to support our natural language prozessing project of pizza menues world wide
The ontology needs to be able to provide the definitions of popular pizza types.

 Background:
 * Test the ontology <http://130.88.198.11/co-ode-files/ontologies/pizza.owl>


 Scenario: Margherita
 In this scenario we test the definition of Margherita.

 Given that my_pizza is a Margherita.
 Then infer that my_pizza has as ingredient some cheese_topping.  
 And infer that my_pizza has as ingredient some tomato_topping.
 Given your_pizza is a meaty_pizza.
 Then infer that my_pizza isn't the same as your_pizza.



 Scenario: insufficient information
 Many pizzas contain cheese and tomato toppings. This alone is not
 sufficient to infer a Marherita.

 Given that my_pizza has as ingredient some cheese_topping.
 Given that my_pizza has as ingredient some tomato_topping.
 Then don't infer that my_pizza is a Margherita.  



 Scenario: La Reine
 we are testing the definition of La Reine pizza by inferring negative information.

 * Infer every La_Reine is a non_vegetarian_pizza.
 And infer that no La_Reine is a Siciliana.
 And infer that no LaReine is a pizza, which has as ingredient some fruit_topping.



 Scenario: Veggi with anchovies
 Veggi pizza should have no fish!

 Given that some pizza is a vegetarian_pizza, which has as ingredient some anchovies_topping.   
 Then the scenario is inconsistent.



 Scenario: Some like it weird
 The ontology allows for weird combinations

 Given that my_food is a cheesey_pizza, which has as ingredient some deep_pan_base.
 And my_food is a spicy_pizza, which has as ingredient some ice_cream.
 And my_food has as country_of_origin England.   
 Then the scenario is consistent.



 Scenario: German pizza

 Given that a German_pizza is defined as a pizza, which has as topping only
 some sausage or sauerkraut.