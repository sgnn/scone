Feature: Scone Test

Background:

Scenario: Ramblings of an idiot (universal Propositions)

Given No dog is a mammal and a bird.
Given No dog is a mammal and a bird and green.
Given No dog is a mammal or a bird or green.
Given No dog is a mammal, which is green
Given No dog is a mammal that is green
Given No dog is a mammal who is green.
Given No dog is a mammal, who is an father of some cat.
Given No dog is a mammal, who is located on  some trees.  
Given No dog is a mammal, that does love some cats.
Given No dog is a mammal who does love only cats.
Given No dog is a mammal that does love cats.
Given Dog and Cat are disjoint.
Given Dog and Cat and Bird are disjoint.


Given Every dog is a mammal.
Given Every DOG is a mam_mal.
Given Every :dog is a ow:mammal

Then don’t infer that John loves Mary.

Scenario: Ramblings on particular propositions

Given that some dogs are cute.
Given that some dogs are cute and lively.
Given that some cats aren’t nice.
Given that some cats aren’t animals, which don’t eat meat.

Scenario: Empty

Scenario:

Scenario: Silly Definitions
Given A foo is defined as sleepy and not hungry.
Given A foo is defined as sleepy or not hungry.
Given A foo is defined as neither sleepy nor hungry.
Given A foo is defined as bear that is hungry or not sleepy.
Given A foo is defined as bear that is hungry and not sleepy.
Given foo is enumerated as Minna and Maya.
Given foo is enumerated as Minna and Maya and Maria.

Then the scenario is inconsistent.

Scenario: More ramblings (Facts)
* Test the ontology <https://ontohub.org/examples/example.owl>

Given John is a mammal and a bird and green.
Given John is a mammal or a bird or green.
Then infer that Mary does love John.
And John isn’t a mammal, which  is a father of some cat.

And John does love Mary.
Given John doesn’t love Mary.
John is the same as Peter.
John isn’t the same as Jill.
John and Jill and James are different.

Then the scenario is consistent.
Then infer John does love Mary.  


Scenario: Verbatim

   Given %owl
   """
   Class: Nut
   Class: Peanut
       SubClassOf: Nut   
   """

Scenario: Verbatim squared

   Given %owl
   """
   Class: Nut
   Class: Peanut
       SubClassOf: Nut   
   """

   Given %owl
   """
   Class: Tree
   Class: Birch
       SubClassOf: Nut   
   """