
# This class is just here to generate new individuals,
# which can be used as wittness for "some" and 
#  "some ... not" sentences 

# puts "wittness loaded"

class Witness

	@@counter = 0 

	def self.indiv() 
		@@counter = @@counter + 1
		return "witness_" + @@counter.to_s
	end
end 


# puts Witness.indiv()
# puts Witness.indiv()
# puts Witness.indiv()
# puts Witness.indiv()

