Feature: Family relationships
  In order to support my research the user should be able query my data
  for family relationships using "male", "female", "parent of",
  "grandparent of", "father of", "mother of", "older than".

Background:
  * Language %OWL
  * Test the ontology <file:///data/resources/Presentation/familyRel.owl>

Scenario: Relative age between family members
  The parenthood relation entails an ordering of age.

  Given Chris is a parent of Dora.
  And Amy is a parent of Chris.
  And Amy is a parent of Berta.
  Then infer that Chris is older than Dora.
  And infer that Amy is older than Dora.
  And don't infer that Berta is older than Dora.
  And don't infer that Dora is older than Dora.

Scenario: Mothers are female

  Given Jill is the mother of Chris.
  Given Jill is not female.

  Then the scenario is inconsistent.

