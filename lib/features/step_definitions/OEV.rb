require 'set'
require 'pp'
require 'logger'
require 'fileutils'
require 'tempfile'
require 'tmpdir'
require_relative '../../parser/scone_parser_api'
require_relative '../../hetsUtils'
require_relative '../../parser/dict'
require_relative '../../aux/DOL_file_builder'

#==============================
# global attributes
#==============================

#Configure the log
$logger = Logger.new(STDOUT)
$logger.level = Logger::INFO
$logger.formatter = proc do |severity, datetime, progname, msg|
 "#{msg}\n"
end

$debug_statement = true

#==============================
# attributes shared across all execution
#==============================
#where to store dol files we generated (Need to end with a "/" )
dolFilesLocation = Dir.mktmpdir('generated-', '/tmp/scone') + '/'
#prefix used for terms introduced in the tests
prefix = ''

=begin This makes the operations on the `hets` object noops.
hets = Object.new()
hets.define_singleton_method(:method_missing) do |*xs|
  return Hash.new(true)
end
=end
hets = HetsUtils.new
parser = SconeParserAPI.new

scone_parser = if($debug_statement)
  SconeParser.new
end

#==============================
# attributes shared across all scenarios of a feature
#==============================

## /TODO/ we do not have a way to discriminate per feature attributes
## (attributes are global to all features and they should not)

#We need a hook that runs after all scenarios have executed.
#We can compare the current scenario_index with the total number of scenarii
total_nb_of_scenarii = 0 #total number of scenarii
scenario_index = 0 #running index of the current

untitled_scenario_count = 0 #count the number of untitled to generate unique dol filename.
truncateLimit = 20 #take only the first 20 characters of the scenario name to generate the filename.

dict = FeatureDictionary.new  #global dictionary of all terms
prefixes = Hash.new #prefix dictionary

init_before = false #flag for an initialization hook before any scenario is run
#flag for an hook after the first scenario parse.
#Allow us to record background info and cache it for reuse by other scenarios of this feature.
cached = false

class OccurenceCounterVisitor

  attr_accessor :type

  def initialize(scenarioName, type)
    @scenarioName = scenarioName
    @type = type
  end

  def incrementCount(dict, term)
    if ! dict[term].has_key?(:count)
      dict[term][:count] = 1
    else
      dict[term][:count] += 1
    end
  end
end

# Hook executed before each scenario execution.
# Setup
Before do |scenario|

  if !init_before
    total_nb_of_scenarii = scenario.feature.feature_elements.size
  end

  init_before = true

  #record the name of this feature (optional)
  @featureName = (scenario.feature.respond_to?(:title) &&
                  scenario.feature.title) || ""

  #record the name of this scenario / generate one if empty
  @scenarioName = select_scenario_name(scenario,untitled_scenario_count)

  $logger.debug "Scenario title is  #{@scenarioName}"
  $logger.debug "Feature title is #{@featureName}"

  #We generate one dol file per scenario. This could be configurable.
  @filename = File.new("#{dolFilesLocation}/#{scenario_index}.dol", "w").path
  $logger.debug "DOL file for feature #{@featureName} scenario #{@scenarioName} will be generated as : #{@filename}"

  # tags info for debugging
  @scenario_tags = scenario.source_tag_names
  $logger.debug "tags are " + @scenario_tags.to_s

  # initiliaze attributes for the current scenario
  @axioms = []
  @prove = []
  @disprove = []
  @imports = []
end

After do |scenario|

 # Cucumber does not provide a hook that would execute once after the background.
 # The solution is then to store the imports data when we execute the 1rst scenario
 # and then cache for other scenarii.
  if ! cached
      # Generate references terms and prefixes referenced in each feature.
      # This is done once and then cached
      #terms are recorded per import to keep track of provenance.
      #prefixes are flatten out in one map. Clash are reported as warnings.
      $logger.debug "imports : \n" + @imports.to_s

      @imports.
      map{|uri|  uri[1,uri.length-2] }. #we remove "<"...">" around the uri (neede for hets)
      each { |i|
            dict.refTerms[i] =  hets.getDictionnary(i)
            pref = hets.getPrefixes(i)
            pref.each{ |k,v|
              if prefixes.has_key?(k)
                logger.warn "import #{i} - Prefix #{key} has already been declared in another import. Value will be overriden."
              end
              prefixes[k] = v
            }
          }

      $logger.debug "generated prefixes : #{prefixes}"

      # generate the reverse map.
      uriToPrefix = Hash.new
      prefixes.each { |k,v|
         uriToPrefix[v] = k
      }
      $logger.debug "uri to prefix : " + uriToPrefix.to_s

      #curate dictionary. We need that to term prefix uri by their actual prefix value
      dict.refTerms.each { |i,terms|
          terms.each { |term , definition|
              $logger.debug "term : "+ definition.to_s
              $logger.debug  definition[:name]
              split = definition[:name].split(":")
              uri = split[0..split.size-2].join(":")
              uri = "<"+uri+'#>'
              $logger.debug  "uri: " + uri.to_s
              definition[:name] = uriToPrefix[uri]+":"+split.last
              $logger.debug  "new def: " + definition.to_s
          }
      }

      #add the global owl:Thing term
      dict.refTerms[:global] = {"thing" => { name: "owl:Thing" , type: "Class"}}

      cached = true
  else
      $logger.debug "no need to generate import data, cache found"
  end

  scenario_index += 1 #increment at each new scenario execution

  $logger.debug "\n #{@scenarioName} - prefixes are : \n" + prefixes.to_s
  $logger.debug "\n #{@scenarioName} - refTerms are : \n" + dict.refTerms.to_s

  newTerms = Hash.new # collect new terms present in this scenario

  #parse statements
  $logger.debug "#{@scenarioName} - parse axioms: " + @axioms.to_s
  @axiomsDOL = @axioms.map do |statement|
    occurenceCounterVisitor = OccurenceCounterVisitor.new(@scenarioName,:AXIOM)
    parser.parse(statement,dict.refTerms,newTerms,occurenceCounterVisitor)
  end

  $logger.debug "#{@scenarioName} - parse prove request" + @prove.to_s
  @proveDOL = @prove.map do |statement|
    occurenceCounterVisitor = OccurenceCounterVisitor.new(@scenarioName,:CQ)
    parser.parse(statement,dict.refTerms,newTerms,occurenceCounterVisitor)
  end

  $logger.debug "#{@scenarioName} - parse disprove request" + @disprove.to_s
  @disproveDOL = @disprove.map do |statement|
    occurenceCounterVisitor = OccurenceCounterVisitor.new(@scenarioName,:CQ)
    parser.parse(statement,dict.refTerms,newTerms, occurenceCounterVisitor)
  end

  # store new terms in the dictionary
  dict.newTerms[@scenarioName] = newTerms

  $logger.debug "#{@scenarioName} - dictionary is  : " + dict.to_s
  $logger.debug "#{@scenarioName} - new terms are  : " + dict.newTerms[@scenarioName].to_s
  $logger.debug "#{@scenarioName} - generating dol file..."

  # we generate axioms for each new term found.
  ## /TODO/ Should be only secondary terms
  addAxioms = []
  dict.newTerms[@scenarioName].each{ |canonical, term|
    addAxioms << "#{term[:type]}: #{prefix}#{term[:name]}"
  }
  $logger.debug "axioms to add : #{addAxioms}"

  file = @filename

  #generate a valid uri for the scenario prefix (optional)
  absolute_path = File.absolute_path(file)
  file_uri = "file:///" + absolute_path.sub("/tmp/scone/", "/data/") + '#'
  $logger.debug "generating uri : #{file_uri}"

  #generate the dol for this scenario. Return the list of nodes to prove to disprove and the scenario node
  fileBuilder = DOLFileBuilder.new( file ,
    {language_id:@language_id, imports: @imports, axioms: @axiomsDOL, prove: @proveDOL , disprove: @disproveDOL, prefixes: prefixes, newTerms: addAxioms, scenario_prefix: "", file_location: file_uri } )
  nodes = fileBuilder.buildFile
  $logger.debug "proveNodes : #{nodes[0]}"
  $logger.debug "disproveNodes : #{nodes[1]}"
  $logger.debug "scenarioNode #{nodes[2]}"

  #list of theorems to prove
  theorems = Hash.new
  nodes[0].each{ |k , v|
    theorems[k] = true
  }
  nodes[1].each{ |k, v|
    theorems[k] = false
  }
  #needed for (in)consistency tests
  scenario_node = nodes[2]

  $logger.info "" # /TODO/ remove

  #prove theorems
  if theorems.size != 0
      @NodesToQuestionsMap = nodes[0].merge(nodes[1])

      $logger.debug "trying to parse #{file} and prove theorems #{theorems}"
      resProve = hets.proveNodes(file,theorems)
      $logger.debug  "result : " + resProve.to_s

      result = Hash.new
      errors = false

      theorems.each { |k,v|
        key = k + " - " + @NodesToQuestionsMap[k]
        if resProve[k]
          value = "success"
        else
          value = "failure"
          errors = true
        end
        result[key] = value
      }

      $logger.debug "result of inferences: \n " + result.to_s

      if(errors)
        # raise "some tests failed : #{result}"
        raise "some tests failed"
      end
  end

  if(@consistency_check)
    resConsistency = hets.checkNodesConsistency(file,{ scenario_node => true})

    if(resConsistency[scenario_node] != true)
      raise "consistency_check failed"
    else
      $logger.info "consistency check succeeded"
    end
  end

  if(@inconsistency_check)
    resInconsistency = hets.checkNodesConsistency(file,{ scenario_node => false})
    if(resInconsistency[scenario_node] != true)
      raise "inconsistency_check failed"
    else
      $logger.info "inconsistency check succeeded"
    end
  end

  ## Hook to be executed after all scenarii have run
  if scenario_index == total_nb_of_scenarii

      $logger.info("\n========================= \nSummary :")

      #Collect all terms not covered in the ontology under test
      newTermsList = "Terms not covered in the ontology under test : "
      newterms_count = 0
      dict.newTerms.each{ |name,scenario|
        scenario.each{ |key, definition|
          if definition[:type] != "Individual"
            newTermsList += "#{definition[:name]}  "
            newterms_count += 1
          end
        }
      }
      $logger.debug "new terms count #{newterms_count}"

      global_count = 0
      used_count = 0
      coverageInfo = "Test Coverage : \n"

      #Collect all terms that are not tested
      termsNotTested = "Some terms are not tested : "
      dict.refTerms.select{|k,v| ! k.to_s.match(/global/)}.each{ |k,v|
        v.each { |term, definition|
          if definition[:type] != "Individual"
              global_count += 1
              if definition.has_key?(:count)
                coverageInfo += "#{definition[:name]} has been used #{definition[:count]} times. \n"
                used_count += 1
              else
                termsNotTested += "#{definition[:name]}  "
                coverageInfo += "#{definition[:name]} has not been used.\n"
              end
          end
        }
      }

      $logger.debug coverageInfo

      #Global coverage info
      global_coverage = used_count.fdiv(global_count) * 100
      $logger.info ""
      $logger.info "global_coverage for feature #{@featureName}: #{global_coverage}%"
      if(global_count != used_count)
        $logger.info "#{termsNotTested}"
      end

      #Ontology range info
      ontology_range = global_count.fdiv( newterms_count + global_count ) * 100
      $logger.info "ontology_range for feature #{@featureName}: #{ontology_range}%"
      if(newterms_count != 0)
        $logger.info "#{newTermsList}"
      end

      $logger.info("=========================")

  end

end


################
# Load Ontology
################


Given (/^(?:that )?(.*)$/i) do |*args|

  # statement is a source block or scone sentence
  if(args.size == 2 ) # we have a source block
    statement = args[0] + " \n" + '"""' + args[1] + " \n" + '"""'
  else # we have a scone sentence
    statement = args[0]
  end

  # classify statement
  classify(statement,scone_parser)
end



