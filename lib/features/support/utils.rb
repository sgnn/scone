require 'logger'


def select_scenario_name(scenario, untitled_scenario_count)
  title = (scenario.respond_to?(:title) && scenario.title) || ""
  if ! title.empty?
    $logger.debug "scenario titled #{title}"
    title
  else
    untitled_scenario_count += 1
    new_title = "scenario#{untitled_scenario_count}"
    $logger.debug "no scenario title, generating one : #{new_title}"
    new_title
  end
end

# method extracted from : http://stackoverflow.com/questions/1939333/how-to-make-a-ruby-string-safe-for-a-filesystem
def sanitize_filename(filename)
  # Split the name when finding a period which is preceded by some
  # character, and is followed by some character other than a period,
  # if there is no following period that is followed by something
  # other than a period (yeah, confusing, I know)
  fn = filename.split /(?<=.)\.(?=[^.])(?!.*\.[^.])/m

  # We now have one or two parts (depending on whether we could find
  # a suitable period). For each of these parts, replace any unwanted
  # sequence of characters with an underscore
  fn.map! { |s| s.gsub /[^a-z0-9\-]+/i, '_' }

  # Finally, join the parts with a period and return the result
  return fn.join '.'
end

DONT = /(?:don't|don’t)/i
DONT_INFER = /^(?:#{DONT} )infer (?:that )?(.*)$/
INFER =/^infer (?:that )?(.*)$/
IS_CONSISTENT =/the\s(scenario|ontology)\sis\sconsistent/i
IS_INCONSISTENT = /the\s(scenario|ontology)\sis\sinconsistent/i
IMPORT = /^(?:Load|Test) (?:the ontology )?([\S]*)$/i
LANGUAGE_ID = /^Language (?:%)?(.*)$/

def classify(statement,parser)

  case
    when statement.match(LANGUAGE_ID)
      @language_id = statement.match(LANGUAGE_ID)[1]
      $logger.debug "language_id is #{@language_id}"
    when statement.match(IMPORT)
      uri = statement.match(IMPORT)[1]
      @imports << uri
      $logger.debug "Ontology #{uri} imported"
    when statement.match(INFER)
      cq = statement.match(INFER)[1]
      @prove << cq
      $logger.debug "Prove request found"
      if($debug_statement) then check_syntax(cq,parser) end
    when statement.match(DONT_INFER)
      cq = statement.match(DONT_INFER)[1]
      @disprove << cq
      $logger.debug "Disprove request found"
      if($debug_statement) then check_syntax(cq,parser) end
    when statement.match(IS_CONSISTENT)
      @consistency_check =true
      $logger.debug "Consistency check found"
    when statement.match(IS_INCONSISTENT)
      @inconsistency_check = true
      $logger.debug "Inconsistency check found"
    else
      @axioms << statement
      $logger.debug "Axiom found"
      if($debug_statement) then check_syntax(statement,parser) end
  end
end


def check_syntax(statement,parser)
  begin
    tree = parser.parse(statement)
    rescue Parslet::ParseFailed => failure
      raise failure.cause.ascii_tree
  end
end
