class SconeParser < Parslet::Parser

    ###################### literals ######################
    rule(:space)  { match('[\s]').repeat(1) }
    rule(:space?) { space.maybe }
    rule(:crlf) { match('[\r\n]').repeat(1) }
    rule(:line_end) { crlf >> space? }
    rule(:eol) { line_end.repeat }

    rule(:_and) { str('and') >> space }
    rule(:nor) { str('nor') >> space }
    rule(:neither) { str('neither') >> space }
    rule(:_or) { str('or') >> space }
    rule(:comma) { str(',').maybe >> space? }
    rule(:the) { str('the') >> space }
    rule(:an) { str('a') >> str('n').maybe  >> space }
    rule(:as) { str('as') >> space }
    rule(:pronoun) { ( str('who') | str('which') | str('that') ) >> space }
    rule(:some) { str('some') }
    rule(:only) { str('only') }

    rule(:does) { str('does') >> space }
    rule(:has) { str('has') >> space }
    rule(:is) { str('is') >> space }

    rule(:_not) { str('not').as(:negation) >> space }
    rule(:doesnot) { ( str('doesn’t') | str('doesn\'t') | str('does not') ).as(:negation) >> space }
    rule(:hasnot) { ( str('hasn’t') | str('hasn\'t') | str('has not') ).as(:negation) >> space }
    rule(:isnot) { ( str('isn’t') | str('isn\'t') | str('is not') ).as(:negation) >> space }

    rule(:quantifier) { ( some | only ).as(:quantifier) >> space }

    ###################### terms ######################
    exclusions = /every|some|no|infer|that/

    rule(:word) { match(/(?!#{exclusions})[\w'’]/).repeat(1)  }

    rule(:class_name) { word.as(:class_name) >> space? }
    rule(:predicate_name) { word.as(:predicate_name) >> space? }
    rule(:predicate_fragment) { word.as(:predicate_fragment) >> space? }

    rule(:pos_class) { (an.maybe >> class_name).as(:pos_class) }
    rule(:class_atom) { ( _not.maybe >> pos_class ).as(:class_atom) }

    rule(:predicate_end) {
      ( str('of') | str('than') | str('to') | str('on') |str('in')
      ).as(:predicate_end) >> space
    }
    rule(:predicate_open) { doesnot | does | ( hasnot | has ) >> as }
    rule(:predicate_1) { predicate_open >> predicate_name }
    rule(:predicate_2) {
      ( isnot | is ) >>
      ( the | an ).maybe >> predicate_fragment >> predicate_end
    }
    rule(:predicate) { (predicate_1 | predicate_2).as(:predicate) }

    rule(:disjunction) {
      ( class_atom >> ( _or >> class_atom ).repeat(1,nil) ).as(:disjunction)
    }

    rule(:conjunction_and) {
      pos_class >> ( _and >> class_atom ).repeat(1,nil)
    }
    rule(:conjunction_neither) {
      neither.as(:negation) >> pos_class >>
      ( nor.as(:negation) >> pos_class ).repeat(1,nil)
    }
    rule(:conjunction) {
      (conjunction_and | conjunction_neither).as(:conjunction)
    }

    rule(:qualifier) {
      (( predicate >> quantifier >> class_expression ) |
       ( is >> class_expression )).as(:qualifier)
    }
    rule(:qualified_class) {
      ( pos_class >> comma.maybe >> pronoun >> qualifier).as(:qualified_class)
    }

    rule(:class_expression) {
      ( conjunction | disjunction | qualified_class | class_atom
      ).as(:class_expression)
    }

    ###################### sentences ######################
    rule(:dot) { str(".") >> space? }
    rule(:delimiter) {  eol >> str('"""') >> space? >> eol }
    rule(:a) { str('a') >> space }
    rule(:no) { str('no') >> space }
    rule(:are) {str('are') >> space }
    rule(:arenot) {
      ( str('aren’t') | str('aren\'t') | str('are not')).as(:negation) >>
      space
    }
    rule(:same_KW) {str('same') >> space }
    rule(:different_KW) {
      # optional space because it can be at the end of line
      str('different') >> space?
    }
    rule(:defined) { str('defined') >> space }
    rule(:enumerated) { str('enumerated') >> space }
    rule(:disjoint) { str('disjoint') >> space? }
    rule(:every) { str('every') >> space }
    rule(:subclass) { str('subclass') >> space? }
    rule(:of) { str('of') >> space }
    rule(:a_subclass_of) { a >> subclass >> of }
    rule(:the_same_as) {
      # Caters for indeterminate amount of white space inside the expression.
      the >> same_KW >> as
    }
    rule(:indiv_name) { word.as(:indiv_name)  >> space? }
    rule(:language_ID) { ( str("%CommonLogic") | str("%OWL") ) >> space? }
    rule(:source_body) {
      ( ( str('"').absent? >> str('"').absent? >> str('"').absent? >> any
        ).repeat().as(:source) ) >> space?
    }

    rule(:indiv_list) { (indiv_name >>  ( _and >> indiv_name).repeat(1,nil) ) }

    rule(:instance) {
      ( indiv_name  >> ( isnot | is ) >> class_expression ).as(:instance)
    }
    rule(:relation) {
      ( indiv_name.as(:indiv1) >> predicate >> indiv_name.as(:indiv2)
      ).as(:relation)
    }
    rule(:different) { ( indiv_list >> are >> different_KW ).as(:different) }
    rule(:equation) {
      ( indiv_name.as(:indiv1) >> ( isnot | is ) >> the_same_as >>
        indiv_name.as(:indiv2)
      ).as(:equation)
    }
    rule(:fact) { (equation | relation | different | instance).as(:fact) }

    rule(:enumerate) { (enumerated >> as >> indiv_list).as(:enumerated) }
    rule(:define) { (defined >> as >> class_expression).as(:defined) }
    rule(:definition) {
      (pos_class >> is >> ( define | enumerate )).as(:definition)
    }

    rule(:universal_negative){
      ( ( no.as(:negation) >> class_name >> is >> class_expression ) |
        ( (class_name >> ( _and >> class_name).repeat(1,nil)
          ).as(:disjoint) >> are >> disjoint )).as(:universal_negative)
    }
    rule(:universal_positive){
      ( ( every.as(:every) >> class_name >> is >> class_expression ) |
        ( class_name >> is >> a_subclass_of.as(:subclass_of) >>
          class_expression)
      ).as(:universal_positive)
    }
    rule(:universal) { universal_positive | universal_negative }
    rule(:particular){
      ( some.as(:some) >> space >> class_name >> (are | arenot) >>
        class_expression
      ).as(:particular)
    }
    rule(:proposition){ (universal | particular).as(:proposition) }
    rule(:source) { space? >> language_ID >> delimiter >> source_body >> delimiter  }

    rule(:infer_sentence) { (definition | universal | fact | source ).as(:infer_sentence) >> space.maybe >> eol }

    rule(:sentence) { (definition | proposition | fact | source ).as(:sentence) >> space.maybe >> dot.maybe >> eol  }

    rule(:text) { sentence.repeat().as(:text) }

    root(:text)

  end
