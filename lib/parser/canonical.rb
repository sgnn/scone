class Canonical

  def canonize(str)
    str.downcase.gsub("_","")
  end

end
