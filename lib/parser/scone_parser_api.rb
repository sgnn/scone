require 'parslet'
require 'pp'
require 'parslet/convenience'
require 'logger'
require_relative 'scone_parser'
require_relative 'scone_parse_transform'



# TODO refactor : split scone_parsing et dol_generation.
# That would allow us to parse the scone first (and fail at the step level if
# an error occurs) and to build the DOL statement afterward (in the After step)

class SconeParserAPI

  def parse(str,refTerms,newTerms, occurenceCounterVisitor)

    begin
        parser = SconeParser.new

        $logger.debug "=============================="
        $logger.debug str

        interm_tree = parser.parse(str)

        $logger.debug "Intermediary tree : \n"
        $logger.debug interm_tree

        transf = SconeParseTransform.new

        context = {
          refTerms: refTerms,
          newTerms: newTerms,
          occurenceCounterVisitor: occurenceCounterVisitor
        }
        $logger.debug "Context : \n" + context.to_s

        dol = transf.apply(interm_tree, context)

        dump = PP.pp dol, dump = ""
        $logger.debug "DOL : #{dump}"
        dol

        rescue Parslet::ParseFailed => failure
        $logger.debug failure.cause.ascii_tree

        $logger.debug "=============================="
    end
  end

 end
