#!/usr/bin/ruby

require 'erb'
require 'net/http'
require 'open3'
require 'io/wait'
require 'logger'

require 'faraday'


class HetsUtils
  def initialize()
  end

  def checkNodesConsistency(inputfilename, consistency_checks)
    resultConsistency = Hash.new
      #for each node in consistency_checks
      consistency_checks.each do |node,expected|
        #check if the node is consistent, and display the result
        result = check(inputfilename, node, "consistency-check")
        if expected then
          expectedResult="consistent"
        else
          expectedResult="inconsistent"
        end
        resultConsistency[node] = false
        case result
          when :consistent
            if expected then
              resultConsistency[node] = true
            end
            $logger.debug "#{node} is consistent (expected : #{expectedResult})"
          when :inconsistent
            if !expected then
              resultConsistency[node] = true
            end
            $logger.debug "#{node} is inconsistent (expected : #{expectedResult})"
          when :error
            $logger.debug "An error occured while checking the consistency of the node #{node}"
        end

      end
    return resultConsistency
  end

  def proveNodes(inputfilename, proof_checks)
    resultProofs = Hash.new
      #for each node in proof_checks
      proof_checks.each do |node,expected|
        #check if the node is proved, and display the result
        result = check(inputfilename, node, "prove")
        if expected then
          expectedResult="proved"
        else
          expectedResult="disproved"
        end
        resultProofs[node]=false
        case result
          when :proved
            if expected then
              resultProofs[node]=true
              $logger.debug "success"
            else
              $logger.debug "failure"
            end
            $logger.info "#{node} is proved (expected : #{expectedResult})"
          when :disproved
            if !expected then
              resultProofs[node]=true
              $logger.debug "success"
            else
              $logger.debug "failure"
            end
            $logger.info "#{node} is disproved (expected : #{expectedResult})"
          when :error
            $logger.debug "An error occured while prooving the node #{node}"
        end
      end
    return resultProofs
  end

  def execute(inputfilename, consistency_checks,proof_checks)
    resultConsistency = Hash.new
    resultProofs = Hash.new
    #Launch hets interactive mode using popen3 to open a shell session
    stdin, stdout, stderr, wait_thr = Open3.popen3('hets -I') do | stdin, stdout, stderr |
      #send the command "use [inputfilename]" to tell hets to use this file
      res = send_cmd("use "+inputfilename, stdin, stdout,true)
      #check if there was an error during the parsing
      if res.downcase.include?("error") then
        $logger.debug "Error during the execution.\n#{res}"
        clean_pipes(stdin, stdout, stderr, wait_thr)
        exit
      end
      #for each node in consistency_checks
      consistency_checks.each do |check|
        #check if the node is consistent, and display the result
        res = check_consistency(check, stdin, stdout)
        case res
          when :consistent
            $logger.debug "#{check} is consistent"
          when :inconsistent
            $logger.debug "#{check} is inconsistent"
          when :error
            $logger.debug "An error occured while checking the consistency of the node #{check}"
        end
        resultConsistency[check]=res
      end
      #for each node in proof_checks
      proof_checks.each do |check|
        #check if the node is proved, and display the result
        res = prove(check, stdin, stdout)
        case res
          when :proved
            $logger.debug "#{check} is proved"
          when :disproved
            $logger.debug "#{check} is disproved"
          when :error
            $logger.debug "An error occured while prooving the node #{check}"
        end
        resultProofs[check]=res
      end
      #send the "quit" command to tell hets we're finished
      send_cmd("quit", stdin, stdout)
      clean_pipes(stdin, stdout, stderr, wait_thr)
    end
    return resultConsistency,resultProofs
  end

  def getDictionnary(uri)
    #Initialize the Dictionnary structure
    dict = Hash.new
    #Clean the /tmp/hetsSig in case there is still some unwanted stuff in it
    system 'rm -R /tmp/hetsSig 2> /dev/null'
    #Recreate the /tmp/hetsSig folder
    system 'mkdir /tmp/hetsSig'
    #Generate the .sig files with hets -v2 -o sig -O <destination> <uri>
    system 'hets -v2 -o sig -O /tmp/hetsSig '+uri+' > /dev/null'
    #Find the file created
    file = `find /tmp/hetsSig/ -name \*.sig`
    #If there is a file
    if file != nil && file != "" then
      #Then open it in read only mode
      File.open(file.strip!, "r") do |f|
        #For each line of the file
        f.each_line do |line|

          $logger.debug "#{line} \n"
          #Parse the line to retrieve the signature
          type,term,ont = parse_signature(line)
          #If it's a signature, then add it to the dictionnary
          if type != nil && term != nil then
            if ont == nil then
              dict[sanitize(term)] = { name: "#{term}" , type: "#{type}" }
            else
              dict[sanitize(term)] = { name: "#{ont}:#{term}" , type: "#{type}" }
            end
          end
        end
      end
    end
    #Remove the temporary folder
    system 'rm -R /tmp/hetsSig'
    #Return the dictionnary
    return dict
  end

  def getPrefixes(uri)
    #Initialize the Prefixes structure
    prefixes = Hash.new
    #Clean the /tmp/hetsSig in case there is still some unwanted stuff in it
    system 'rm -R /tmp/hetsSig 2> /dev/null'
    #Recreate the /tmp/hetsSig folder
    system 'mkdir /tmp/hetsSig'
    #Generate the .sig files with hets -v2 -o sig -O <destination> <uri>
    system 'hets -v2 -o sig -O /tmp/hetsSig '+uri+' > /dev/null'
    #Find the file created
    file = `find /tmp/hetsSig/ -name \*.sig`
    #If there is a file
    if file != nil && file != "" then
      #Then open it in read only mode
      File.open(file.strip!, "r") do |f|
        #For each line of the file
        f.each_line do |line|
          #Parse the line to retrieve the prefix
          prefix_name,prefix_uri = parse_prefixes(line)
          #If it's a prefix, then add it to the prefixes
          if prefix_name != nil && prefix_uri != nil then
            prefixes[prefix_name] = prefix_uri.delete("\n")
          end
        end
      end
    end
    #Remove the temporary folder
    system 'rm -R /tmp/hetsSig'
    return prefixes
  end

  private

  def compareSignatures(termsOrig,termsHets,types)
    resultSig = Hash.new
    if types != nil then
      types.split(",").each do |type|
        if termsOrig[type] != nil then
          termsOrig[type].split(",").each do |term|
            res = false
            if termsHets[type].include? sanitize(term) then
              res = true
            end
            resultSig["#{type}_#{term}"] = res
          end
        end
      end
    end
    return resultSig
  end

  #This method removes from the input string the whitespaces, the underscores ant $logger.debug it in lower case
  def sanitize(input)
    return input.downcase.delete(' ').delete('_')
  end

  def parse_signature(line)
    #If it's a Class or ObjectProperty entry
    if line.start_with?("Class","ObjectProperty","Individual") then
      #Then identify the type
      if line.start_with?("Class") then
        type = "Class"
      elsif line.start_with?("Individual") then
        type = "Individual"
      else
        type = "ObjectProperty"
      end
      #If the lines looks like "Anything: <anything#anything>" (case insensitive) then
      if line =~ /.*:\s<.*#.*>/i then
        #Split the input line to get an array that looks like ["what's before the #","what's after the # - the term"]
        splittedLine = line.split('#')
        #If the array contains at least two terms
        if splittedLine!=nil && splittedLine.length >= 2 then
          #Identify the term that is the last entry of the array, identify the ontlology prefix and send back the type, the sanitized term and the ontology prefix
          term=splittedLine.last
          if term != nil then
            term=term[0,term.length-2]
          #  ont = splittedLine[0].split("/").last
            ont = splittedLine[0].split("<")[1]
            return type,term,ont
          end
        end
      #Else if it doesn't match the regexp
      else
        #If the lines looks like "Type : Prefix:Term" or "Type : Term"
        splittedLine = line.split(": ")
        if splittedLine!=nil && splittedLine.length > 0 then
          #Then identify the second part of the string and parse it to get
          term = splittedLine.last.delete("\n")
          #Send back the type and the term, plus if the term has an ontology prefix, send it otherwise send nil
          splittedLine = term.split(":")
          if splittedLine.length==2 then
            return type,splittedLine.first,splittedLine.last
          else
            return type,term,nil
          end
        end
      end
    end
  end

  def parse_prefixes(line)
    #If the line is a prefix entry
    if line.start_with?("Prefix") then
      #Split the line that looks like "Prefix: ont: <uri_to_the_ont>" to separate the terms and get this array : ["Prefix","ont","<uri_to_the_ont>"]
      splittedLine = line.split(": ")
      #If the splittedLine array contains exactly 3 elements (see example string above)
      if splittedLine!=nil && splittedLine.length == 3 then
        #Then send back the prefix name and the uri
        prefix_name = splittedLine[1]
        prefix_uri = splittedLine[2]
        return prefix_name,prefix_uri
      end
    end
  end

  #This function sends a command in the stdin pipe and returns the output read on the stdout pipe
  def send_cmd(cmd,stdin,stdout,wait=false,waitfor1=nil, waitfor2=nil)
    stdin.puts cmd
    if wait then
      out = ""
      loop do
        sleep 0.03
        if stdout.ready?
          out1 = stdout.read_nonblock(1000000)
          out << out1
          if waitfor1.nil? or out.include?(waitfor1) or waitfor2.nil? or
            out.include?(waitfor2)
            return out
          end
        end
      end
    else
      return ""
    end
  end

  #This function checks if a node is consistent or not
  def check(filename, node, cmd)
    host="localhost"
    port="8000"
    file=ERB::Util.url_encode("file://#{filename.sub("/tmp/scone/", "/data/")}")
    options="auto/full-theories/full-signatures"
    reasoner = {"prove" => "Fact", "consistency-check" => "darwin"}
    conjecture = {"prove" => '"conjecture": "Ax1",', "consistency-check" => nil}
    data = <<~json
    {
      "format": "json",
      "goals":
      [
        {
          "node": "#{node}",
          #{conjecture[cmd]}
          "reasonerConfiguration":
          {
            "timeLimit": 0,
            "reasoner": "#{reasoner[cmd]}"
          },
          "premiseSelection":
          {
            "kind": "sine",
            "sineDepthLimit": 2,
            "sineTolerance": 1,
            "sinePremiseNumberLimit": 50
          }
        }
      ]
    }
    json

    if node==nil then
      $logger.debug "Error : node is null"
      return :error
    end
    #Send the checking request
    $logger.debug("Running #{cmd} on #{filename}:#{node}.")
    response = Faraday.new("http://#{host}:#{port}").post do |request|
      request.url("http://#{host}:#{port}/#{cmd}/#{file}/#{options}")
      request.headers["Content-Type"] = "application/json"
      request.body = data
    end
    #If the result of the command contains the string "is consistent"
    negative = {
      "consistency-check" => {
        :marker => "Inconsistent",
        :result => :inconsistent},
      "prove" => {:marker => "Disproved", :result => :disproved}}
    positive = {
      "consistency-check" => {:marker => "Consistent", :result => :consistent},
      "prove" => {:marker => "Is Proved", :result => :proved}}
    if response.body.include?(positive[cmd][:marker]) then
      #Then return consistent
      return positive[cmd][:result]
    else
      #Else if the result of the command contains the string "inconsistent"
      if response.body.include?(negative[cmd][:marker]) then
        #Then return inconsistent
        return negative[cmd][:result]
      else
        #Otherwise an error occured
        $logger.debug "Error : #{response.body}"
        return :error
      end
    end
  end

  def clean_pipes(stdin, stdout, stderr, wait_thr)
    stdin.close  # stdin, stdout and stderr should be closed explicitly in this form.
    stdout.close
    stderr.close
  end

end
